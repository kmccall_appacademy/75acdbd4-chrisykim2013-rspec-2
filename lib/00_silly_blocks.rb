require 'byebug'

def reverser(&prc)
  prc.call.split.map {|word| word.reverse}.join(" ")
end

def adder(num = 1, &prc)
  prc.call + num
end

def repeater(num = 1, &prc)
  num.times {prc.call}
end
