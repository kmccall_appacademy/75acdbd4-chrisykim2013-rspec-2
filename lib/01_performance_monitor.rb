def measure(num = 1, &prc)
  time_beginning = Time.now
  num.times {prc.call}
  time_end = Time.now
  (time_end - time_beginning)/num
end
